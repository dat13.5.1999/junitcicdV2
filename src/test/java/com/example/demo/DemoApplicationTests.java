package com.example.demo;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void homeResponse() {
		String a = "a";
		String b = "a";
		assertSame(a, b);
	}

	private final Calculator calculator = new Calculator();

	@Test
	public void assertEqualsTest(){
		assertEquals(2, calculator.add(1, 1));
	}

	@Test
	public void assertNotEqualsTest(){
		assertNotEquals(1, calculator.add(1, 1));
	}

	@Test
	public void assertTrueTest(){
		assertTrue((calculator.add(1, 1) == 2));
	}

	@Test
	public void assertFalseTest(){
		assertFalse((calculator.add(1, 1) != 2));
	}

	@Test
	public void assertNullTest(){
		String a = null;
		assertNull(a);
	}

	@Test
	public void assertNotNullTest(){
		String a = "a";
		assertNotNull(a);
	}

	@Test
	public void assertSameTest(){
		String a = "a";
		String a2 = "a";
		assertSame(a, a2);
	}

	@Test
	public void assertNotSameTest(){
		String a = "a";
		String b = "b";
		assertNotSame(a, b);
	}

	@Test()
	public void assertDoesNotThrowTestSuccess(){
		assertDoesNotThrow(() -> {
			int number = 69/3;
			System.out.println("assertThrows Test case success: " + number);
		});
	}
	

	@Test()
	public void assertAllTestSuccess(){
		assertAll(
				() -> assertEquals(2, calculator.add(1, 1)),
				() -> assertNotEquals(3, calculator.add(1, 1))
		);
	}

}

